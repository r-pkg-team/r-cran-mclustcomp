Source: r-cran-mclustcomp
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-mclustcomp
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-mclustcomp.git
Homepage: https://cran.r-project.org/package=mclustcomp
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcpp,
               r-cran-rdpack,
               r-cran-rcpparmadillo
Testsuite: autopkgtest-pkg-r

Package: r-cran-mclustcomp
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Measures for Comparing Clusters
 Given a set of data points, a clustering is defined as a disjoint
 partition where each pair of sets in a partition has no overlapping
 elements. This package provides 25 methods that play a role somewhat
 similar to distance or metric that measures similarity of two
 clusterings - or partitions. For a more detailed description, see Meila,
 M. (2005) <doi:10.1145/1102351.1102424>.
